{% block globals %}{% endblock %}

Name:		{{ pkg.name }}	
{% block version %}
Version:	{{ pkg.version }}
{% endblock %}
Release:	%mkrel 1
Summary:	{{ pkg.summary | truncate(60)}}

License:	{{ pkg.licenses }}
URL:		{{ pkg.homepage }}
Source0:	{{ pkg.source0 }}
BuildArch:	noarch

{% block description %}
%description
TODO
{% endblock %}

{% block requirements %}{% endblock %}

{% block prep %}
%prep
TODO
{% endblock %}

{% block build %}{% endblock %}

{% block install %}{% endblock %}

{% block check %}{% endblock %}
