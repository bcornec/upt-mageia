{% extends 'base.spec' %}
{% block globals %}
%global srcname {{ pkg.sourcename }}
{% endblock %}
{% block version %}
Version:	{{ pkg.version | modversion }}
{% endblock %}

{% block requirements %}
Summary:	{{ pkg.summary }}
    {% for build_dep in pkg.build_depends %}
BuildRequires:	{{ build_dep }}
    {% endfor %}
# Tests
    {% for test_dep in pkg.test_depends %}
BuildRequires:	{{ test_dep }}
    {% endfor %}

    {% for run_dep in pkg.run_depends %}
Requires:	{{ run_dep }}
    {% endfor %}
{% endblock requirements%}

{% block description %}
%description
perl Module for ...
{% endblock description%}

{% block prep %}
%prep
%setup -q -n %{srcname}-%{version}
{% endblock %}

{% block build %}
%build
perl Makefile.PL INSTALLDIRS=vendor
%make %{?_smp_mflags}
{% endblock %}

{% block install %}
%install
%{__rm} -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install
find ${RPM_BUILD_ROOT} -type f -name perllocal.pod -o -name .packlist -o -name '*.bs' -a -size 0 | xargs rm -f
find ${RPM_BUILD_ROOT} -type d -depth | xargs rmdir --ignore-fail-on-non-empty

%files
%doc README

%{perl_vendorlib}/*
%{_mandir}/man3/*
{% endblock %}
