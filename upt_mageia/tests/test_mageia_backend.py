# Copyright 2018      Bruno Cornec
#
# Licensed under AGPLv3 license. See the LICENSE file.
import unittest

import upt

from upt_mageia.upt_mageia import MageiaBackend


class TestMageiaBackend(unittest.TestCase):
    def setUp(self):
        self.mageia_backend = MageiaBackend()

    def test_unhandled_frontend(self):
        upt_pkg = upt.Package('foo', '42')
        upt_pkg.frontend = 'invalid backend'
        with self.assertRaises(upt.UnhandledFrontendError):
            self.mageia_backend.create_package(upt_pkg)


if __name__ == '__main__':
    unittest.main()
